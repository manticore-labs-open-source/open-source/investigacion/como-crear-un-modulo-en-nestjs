import { Get, Controller, Post, Body, Query } from '@nestjs/common';
import { AppService } from './app.service';
import { EjemploService } from 'servicio/ejemplo.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly ejemploService: EjemploService) { }

  @Get('respuesta')
  funcionReaccionarGet(
    @Query('numero1') numero1,
    @Query('numero2') numero2
  ) {
    return this.ejemploService
      .sumar(parseInt(numero1), parseInt(numero2));
  }

  @Post('enviar-data')
  metodoPost(
    @Body('user') user
  ) {
    console.log(user)
  }
}
