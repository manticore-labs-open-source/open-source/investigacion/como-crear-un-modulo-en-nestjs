import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EjemploService } from 'servicio/ejemplo.service';
import { ModuloEjemploModule } from 'modulo-ejemplo/modulo-ejemplo.module';

@Module({
  imports: [
    ModuloEjemploModule
  ],
  controllers: [AppController],
  providers: [
    AppService,
    EjemploService
  ],
})
export class AppModule {}
