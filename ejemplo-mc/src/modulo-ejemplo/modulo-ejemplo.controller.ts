import { Controller, Get } from "@nestjs/common";
import { ModuloEjemploService } from "./modulo-ejemplo.service";

@Controller('modulo-ejemplo')
export class ModuloEjemploController{
    
    constructor(
        private readonly _moduloEjemploService: ModuloEjemploService
        ){}

    @Get()
    obtenerModulo(){
        return this._moduloEjemploService.obtenerNombre();
    }

}