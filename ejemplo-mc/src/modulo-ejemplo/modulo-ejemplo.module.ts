import { Module } from "@nestjs/common";
import { ModuloEjemploController } from "./modulo-ejemplo.controller";
import { ModuloEjemploService } from "./modulo-ejemplo.service";

@Module({
    imports: [],
    controllers: [
        ModuloEjemploController
    ],
    providers: [
        ModuloEjemploService
    ]
})
export class ModuloEjemploModule{

}