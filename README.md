# Como crear un modulo en nestjs

Cuando se necesitan tener diferentes rutas para consultas en backend podemos crear un módulo en nestjs que contenga su propio controller, servicio y modulo que debe ser importado en el modulo principal. En este módulo tambien se pueden crear las tablas de las bases de datos de lo que hablaremos en otra entrada.

Utilizaremos el **ejemplo de nestjs** creadas en entradas anteriores.

Para crear el modulo creamos una carpeta con el nombre del modulo en este caso "modulo-ejemplo" y dentro de este crearemos un archivo service, module y controller de la siguiente manera

![](imagenes/directorio1.png)

Y en cada uno de los archivos escribimos el siguiente código.

**modulo-ejemplo.service.ts**
``` TypeScript
import { Injectable } from "@nestjs/common";

@Injectable()
export class ModuloEjemploService{

    obtenerNombre(){
        return 'Servicio Modulo Ejemplo'
    }
}
```

**modulo-ejemplo.controller.ts**
``` TypeScript
import { Controller, Get } from "@nestjs/common";
import { ModuloEjemploService } from "./modulo-ejemplo.service";

@Controller('modulo-ejemplo')
export class ModuloEjemploController{
    
    constructor(
        private readonly _moduloEjemploService: ModuloEjemploService
        ){}

    @Get()
    obtenerModulo(){
        return this._moduloEjemploService.obtenerNombre();
    }

}
```

**modulo-ejemplo.module.ts**
``` TypeScript
import { Module } from "@nestjs/common";
import { ModuloEjemploController } from "./modulo-ejemplo.controller";
import { ModuloEjemploService } from "./modulo-ejemplo.service";

@Module({
    imports: [],
    controllers: [
        ModuloEjemploController
    ],
    providers: [
        ModuloEjemploService
    ]
})
export class ModuloEjemploModule{

}
```

Adicionalmente debemos agregar el modulo al archivo app.module de la siguiente manera.

**app.module.ts**
``` TypeScript
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EjemploService } from 'servicio/ejemplo.service';
import { ModuloEjemploModule } from 'modulo-ejemplo/modulo-ejemplo.module';

@Module({
  imports: [
    ModuloEjemploModule
  ],
  controllers: [AppController],
  providers: [
    AppService,
    EjemploService
  ],
})
export class AppModule {}
```

Levantamos el servidor con:
```
npm run start
```

y vemos los resultados.

![](imagenes/modulo-ejemplo-run.png)

Eso es todo. Como se puede observar es similar a un módulo en Angular.

<a href="https://twitter.com/atpsito" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @atpsito </a><br>
<a href="https://www.linkedin.com/in/alexander-tigselema-ba1443124/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Alexander Tigselema</a><br>
<a href="https://www.instagram.com/atpsito" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> atpsito</a><br>